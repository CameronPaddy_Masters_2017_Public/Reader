#include "stdafx.h"
#include "TestList.h"

ListNode* create(TCHAR *filename, ListNode* next)
{
	size_t nodesize = sizeof(ListNode);
	ListNode *new_node = (ListNode*) malloc(nodesize);
	if (new_node == NULL)
	{
		printf("Error creating a new node.\n");
		exit(0);
	}
	new_node->filename = malloc(MAX_PATH);
	wcscpy(new_node-> filename, filename);
	new_node->next = next;

	return new_node;
}

void dispose_list(List *list)
{
	ListNode *cursor, *tmp;

	if (list->head != NULL)
	{
		cursor = list->head->next;
		list->head->next = NULL;
		while (cursor != NULL)
		{
			tmp = cursor->next;
			free(cursor->filename);
			free(cursor);
			cursor = tmp;
		}
	}
}

void init_list(List *list) {
	list->size = 0;
	list->head = NULL;
}

int list_size(List *list)
{
	return list->size;
}

void prepend_list(List *list, TCHAR *filename)
{
	ListNode* new_node = create(filename, list->head);
	list->head = new_node;
	list->size++;
}

void remove_any(List *list, ListNode* nd)
{
	if (nd == NULL)
		return NULL;
	/* if the node is the first node */
	if (nd == list->head)
	{
		remove_front(list);
		goto end;
	}

	if (nd->next == NULL)
	{
		remove_back(list);
		goto end;
	}

	if (list->size > 1)
	{
		/* if the node is in the middle */
		ListNode* cursor = list->head;
		while (cursor != NULL)
		{
			if (cursor->next == nd)
				break;
			cursor = cursor->next;
		}

		if (cursor != NULL)
		{
			ListNode* tmp = cursor->next;
			cursor->next = tmp->next;
			tmp->next = NULL;
			free(tmp);
		}
		list->size--;
	}
end: return;
}

void remove_back(List *list)
{
	if (list->head == NULL)
		return NULL;

	ListNode *cursor = list->head;
	ListNode *back = NULL;
	while (cursor->next != NULL)
	{
		back = cursor;
		cursor = cursor->next;
	}

	if (back != NULL)
		back->next = NULL;

	/* if this is the last node in the list*/
	if (cursor == list->head)
		list->head = NULL;

	free(cursor);
	list->size--;
}

void remove_front(List *list)
{
	if (list->head == NULL)
		return NULL;
	ListNode *front = list->head;
	list->head = list->head->next;
	front->next = NULL;
	/* is this the last node in the list */
	if (front == list->head)
		list->head = NULL;
	free(front);
	list->size--;
}

void remove_index(List *list, int index) {
	int i;
	ListNode *tmp;
	tmp = list->head;

	for (i = 0; i < list_size(list); i++)
	{
		if (i == index)
		{
			remove_any(list, tmp);
			break;
		}
		tmp = tmp->next;
	}
}

ListNode *select_node(List *list, int index)
{
	int i;
	ListNode *tmp;
	tmp = list->head;

	for (i = 0; i < list_size(list); i++)
	{
		if (tmp == NULL)
			return NULL;

		if (i == index)
			return tmp;

		tmp = tmp->next;
	}

	return NULL;
}